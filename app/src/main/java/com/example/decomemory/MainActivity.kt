package com.example.decomemory

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.SplashTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val playButton = findViewById<Button>(R.id.playGameButton)
        val helpButton = findViewById<Button>(R.id.helpButton)
        val dificultyNormalButton = findViewById<Button>(R.id.dificulty_normal_button)
        val dificultyHardButton = findViewById<Button>(R.id.dificulty_hard_button)

        var hardMode = false

        val context: Context = applicationContext

        playButton.setOnClickListener {
            if (hardMode){
                val intent = Intent(this, GameScreenHard::class.java)
                startActivity(intent)

            }else {

                val intent = Intent(this, GameScreen::class.java)
                startActivity(intent)
            }
        }

        helpButton.setOnClickListener {
            Toast.makeText(this, "Clic on the cards to revel the icon and try to make pairs", Toast.LENGTH_SHORT).show()
        }

        dificultyNormalButton.setOnClickListener(){
            hardMode = false
            Toast.makeText(this, "Normal mode activated", Toast.LENGTH_SHORT).show()
        }

        dificultyHardButton.setOnClickListener(){
            hardMode = true
            Toast.makeText(this, "Hard mode activated", Toast.LENGTH_SHORT).show()
        }


    }

}