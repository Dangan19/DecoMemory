package com.example.decomemory

import android.util.Log
import androidx.lifecycle.ViewModel

class GameViewModel: ViewModel() {
    // Game data model
    var imatges = arrayOf(
        R.drawable.muisc,
        R.drawable.muisc,
        R.drawable.phone,
        R.drawable.phone,
        R.drawable.disco,
        R.drawable.disco
    )

    var cartes = mutableListOf<Carta>()
    var score = 0
    val cartaBuida = Carta(-1,-1)
    var cartesGirades2 = mutableListOf(cartaBuida, cartaBuida)
    var movements = 0

    // Aquesta funció es estàndard de cada classe, i és com el constructor

    init {
        setDataModel()
    }

    // Funció que barreja les imatges de les cartes i crea l'array de Cartes
    private fun setDataModel() {
        imatges.shuffle()

        for (i in 0..5) {
            cartes.add(Carta(i, imatges[i]))
        }
    }

    // Funció que comprova l'estat de la carta, el canvia i envia
    // a la vista la imatge que s'ha de pintar
    fun girarCarta(idCarta: Int,): Int {

        if (!cartes[idCarta].girada) {
            cartes[idCarta].girada = true


            if (cartesGirades2[0] === cartaBuida){
                   cartesGirades2[0] = cartes[idCarta]

            }else{
               cartesGirades2[1] = cartes[idCarta]

                if (compararCarta(cartesGirades2[0], cartesGirades2[1])){
                    Log.d("!", "YES")
                    resetCarta()
                    score++
                    movements++


                }else{
                    Log.d("!", "NO")
                    CartaDelReves()
                    resetCarta()
                    movements++


                }

            }


            return cartes[idCarta].resId


        } else {
            cartes[idCarta].girada = false
            return R.drawable.reverse_card
        }
    }

    fun CartaDelReves(): Int{
        cartes[cartesGirades2[0].id].girada = false
        cartes[cartesGirades2[1].id].girada = false
            return R.drawable.reverse_card

    }


    // Funció que posa l'estat del joc al mode inicial
    fun resetEstatJoc() {
        for (i in 0..5) {
            cartes[i].girada = false
        }
    }

    // Funció que retorna l'estat actual d'una carta
    fun estatCarta(idCarta: Int): Int {
        if (cartes[idCarta].girada) return cartes[idCarta].resId
        else return R.drawable.reverse_card
    }

    fun isGirada(id: Int): Boolean{
       return cartes[id].girada
    }


    fun compararCarta(id: Carta, id2: Carta) : Boolean {

        if (id.resId == id2.resId ){
            return true
        }
        return false
    }

    fun resetCarta(){
        cartesGirades2[0] = cartaBuida
        cartesGirades2[1] = cartaBuida


    }

    fun movements(): Int {
        return movements

    }
}
