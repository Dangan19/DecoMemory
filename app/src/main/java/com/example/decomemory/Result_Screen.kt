package com.example.decomemory

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider

class Result_Screen: AppCompatActivity() {
    private lateinit var textViewResult: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        val extra: String? = intent.getStringExtra("1")

        val playAgainButton = findViewById<Button>(R.id.playAgain)
        val backButton = findViewById<Button>(R.id.backButton)
        textViewResult = findViewById(R.id.textViewResult)

        textViewResult.text = extra

        playAgainButton.setOnClickListener {
            val intent = Intent(this, GameScreen::class.java)
            startActivity(intent)
        }

        backButton.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

    }
}