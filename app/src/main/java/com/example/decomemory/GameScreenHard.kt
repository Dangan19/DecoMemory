package com.example.decomemory

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider


// ViewModel declaration
lateinit var viewModelHard : GameViewModelHard

class GameScreenHard : AppCompatActivity(), View.OnClickListener {

    // UI Views
    private lateinit var carta1: ImageView
    private lateinit var carta2: ImageView
    private lateinit var carta3: ImageView
    private lateinit var carta4: ImageView
    private lateinit var carta5: ImageView
    private lateinit var carta6: ImageView
    private lateinit var carta7: ImageView
    private lateinit var carta8: ImageView
    private lateinit var resetButton: Button

    private lateinit var textMovement: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_hard_screen)


            viewModelHard = ViewModelProvider(this).get(GameViewModelHard::class.java)

            carta1 = findViewById(R.id.carta1)
            carta2 = findViewById(R.id.carta2)
            carta3 = findViewById(R.id.carta3)
            carta4 = findViewById(R.id.carta4)
            carta5 = findViewById(R.id.carta5)
            carta6 = findViewById(R.id.carta6)
            carta7 = findViewById(R.id.carta7)
            carta8 = findViewById(R.id.carta8)

            textMovement = findViewById(R.id.textMovements)

            //resetButton = findViewById(R.id.reset_button)

            carta1.setOnClickListener(this)
            carta2.setOnClickListener(this)
            carta3.setOnClickListener(this)
            carta4.setOnClickListener(this)
            carta5.setOnClickListener(this)
            carta6.setOnClickListener(this)
            carta7.setOnClickListener(this)
            carta8.setOnClickListener(this)

        /*
            resetButton.setOnClickListener {
                viewModel.resetEstatJoc()
                updateUI()
            }

         */

            updateUI()

        }

    override fun onClick(v: View?) {
            when (v) {

                carta1 -> girarCarta(0, carta1)
                carta2 -> girarCarta(1, carta2)
                carta3 -> girarCarta(2, carta3)
                carta4 -> girarCarta(3, carta4)
                carta5 -> girarCarta(4, carta5)
                carta6 -> girarCarta(5, carta6)
                carta7 -> girarCarta(6, carta7)
                carta8 -> girarCarta(7, carta8)


            }
    }



        // Funció que utilitzarem per girar la carta
        private fun girarCarta(idCarta: Int, carta: ImageView) {
            textMovement.text = "MOVEMENTS: "+ viewModelHard.movements
            updateUI()

            if (viewModelHard.isGirada(idCarta)){
                Log.d("!", "La carta ya esta girada")
                Toast.makeText(this, "This card is already complete!", Toast.LENGTH_SHORT).show()


            }else{

                carta.setImageResource(viewModelHard.girarCarta(idCarta))
            }

            if (viewModelHard.score>=4){
                val intent = Intent(this, Result_Screen::class.java)
                intent.putExtra("1", viewModelHard.movements.toString())
                startActivity(intent)

            }



        }

        // Funció que restauarà l'estat de la UI
        private fun updateUI() {
            carta1.setImageResource(viewModelHard.estatCarta(0))
            carta2.setImageResource(viewModelHard.estatCarta(1))
            carta3.setImageResource(viewModelHard.estatCarta(2))
            carta4.setImageResource(viewModelHard.estatCarta(3))
            carta5.setImageResource(viewModelHard.estatCarta(4))
            carta6.setImageResource(viewModelHard.estatCarta(5))
            carta7.setImageResource(viewModelHard.estatCarta(6))
            carta8.setImageResource(viewModelHard.estatCarta(7))

        }

    }


